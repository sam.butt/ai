# Machine Learning Projects Summary:

1.	Spam classification using KNN and Logistic Regression:
* Spam email classification containing frequency of various words, characters etc for a total of 2740 e-mails. We have training the model for logistic regression and KNN and predicted new email as spam or non-spam. Evaluated the prediction efficiency of both models by using comparing the error rates.

2.	Inference about lifetime of machines
* Infer about machines lifetime by using Bayesian approach to determine the machine’s warranty time.

3.	Perform Linear Discriminant Analysis on Australian Crabs Data.
* Analyze on which features we should use to train the models for LDA and Logistic regression. Predict values, draw decision boundary in the classification data. Also analyze the model’s misclassification rate.

4.	Best Suitable Model for the Loan Company
* Train a model that can be used to predict whether or not a new customer is likely to pay back the loan. Used optimal decision tree and Naïve Bayes to perform classification. Make ROC curve to analyze the models TPR/FPR to choose the model with highest TPR. Loss matrix implementation on Naïve Bayes model to decrease FPR(False Positive Rate).

5.	Uncertainty estimation of the model
* used two different types of bootstrapping (parametric and non-parametric) to measure the uncertainty of our model.

6.	Principal components
* Investigated how the measured spectra can used to predict the viscosity from the data contained near-infrared spectra and viscosity levels for a collection of diesel fuels.

7.	Weather Forecast
* Weather forecasting for 4 seasons in Linkoping for a specific period, e.g. 4 to 24 with the 2 hours difference by using kernel methods.

8.	Neural Network
* Trained a neural network on the given data to learn the trigonometric sine function.

9.	Ensemble Method
* Evaluated the performance of Adaboost classification trees and random forests on the spam data which contains the frequency of various words, characters, etc. for a total of 4601 e-mails.

10.	Examine the mortality rate using time series plots GAM and GLM.
* By using influenza data analyzed and checked the correlation of one variable on other variable using time series plot and fit a GAM model and GLM model and compare which model is working fine. 

11.	High-dimensional methods
* The Data contains information about 64 e-mails which were manually collected from DB World mailing list and classified as (1:Conference, 0:everything else). Fit a nearest shrunken centroid modelol, Ealistic Net and SVM and compare which model is less complex with the less error classification rate.
